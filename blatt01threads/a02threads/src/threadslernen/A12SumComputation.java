package threadslernen;

public class A12SumComputation {

	final static int SIZE = 100000;
	private int sum = 0;
	private int[] sa1 = new int[SIZE];
	private int[] sa2 = new int[SIZE];
	private long starttime;
	private long endtime;

	public static void main(String[] args) {
		System.out.println("Diese Lösung wurde erstellt von Oraz Serdarov\n");
		A12SumComputation prog = new A12SumComputation();
		if (args.length == 0) {
			System.out.println("Programm wird ohne Synchronization gestartet \n --------------------------------\n");		
			prog.execute(args);
		} else {
			System.out.println("Programm wird mit Synchronization gestartet \n --------------------------------\n");
			prog.execute(args);
		}

		System.out.println("--------------------------------\n Es hat " + ( prog.endtime- prog.starttime ) +"gedauert.");
		
	}

	 synchronized void addToSum(int i) {
		this.sum = this.sum + i;
	}

	private void execute(String[] args) {
		System.out.println("Starte Programm A12SumComputation\n");
		this.starttime =System.currentTimeMillis();
		for (int i = 0; i < SIZE; i++) {
			sa1[i] = 1;
			sa2[i] = 2;
		}
		Thread worker1 = new A12Worker("1");
		Thread worker2 = new A12Worker("2");
		if(args.length>0) {
		worker1.start();
		worker2.start();

		try {
			worker1.join(); 
			worker2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		}else {
			worker1.run();
			worker2.run();
		}
		System.out.println("Programm: sum= " + sum);
		System.out.println("Beende Programm A12SumComputation");
		
		this.endtime=System.currentTimeMillis();
	}// execute

	private class A12Worker extends Thread {

		private A12Worker(String name) {
			this.setName(name);
		}

		@Override
		public void run() {
			System.out.println("Starte worker " + this.getId() + " der Klasse " + this.getClass().getName());

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			if (this.getName().equals("1")) {

				for (int i = 0; i < SIZE; i++) {
					addToSum(sa1[i]);
				}

			} else {
				for (int i = 0; i < SIZE; i++) {
					addToSum(sa2[i]);
				}
			}

			System.out.println("Worker " + this.getId() + " : sum = " + sum);
			System.out.println("Beende worker " + this.getId() + " der Klasse " + this.getClass().getName());

		}

	}
}
