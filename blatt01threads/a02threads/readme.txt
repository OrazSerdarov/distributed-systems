Autor: Oraz Serdarov
Verteilte Systeme SS 2020
Blatt 01: Arbeitsumgebung Linux, Threads, Synchronisation
Aufgabe 02
Vorbedingung:
Das aktuelle Arbeitsverzeichnis ist
blatt01threads/a02threads

Generieranleitung:
1. Generieren des ausführbaren Programms mit
javac -d build/classes/ src/threadslernen/A12SumComputation.java

Installationsanleitung:
Entfällt. Die Dateien sind direkt nach Generierung ausführbar.

Bedienungsanleitung:
1a. Start des ausführbaren Programms mit Synchronisation
java -cp build/classes -Djava.compiler=NONE threadslernen/A12SumComputation zeichenkette

1b. Start des ausführbaren Programms ohne Synchronisation
java -cp build/classes -Djava.compiler=NONE threadslernen/A12SumComputation 

