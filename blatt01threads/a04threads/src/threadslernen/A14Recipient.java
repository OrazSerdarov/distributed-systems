package threadslernen;

import java.util.Date;
import java.util.concurrent.Callable;

public class A14Recipient implements Callable<Boolean> {

	private String name;
	private A14MsgBufferInf bf;
	Date date;
	private int count;

	public A14Recipient(String name, A14MsgBufferInf bf) {
		this.name = name;
		this.bf = bf;
		this.date = new Date();
		this.count = 0;
	}

	@Override
	public Boolean call() throws Exception {
		System.out.println("\t Worker(" + this.getName() + ") wird gestartet...");

		A14Msg aMsg;
		while (!Thread.interrupted()) {

			try {
				synchronized (bf) {
					if (bf.isEmpty()) {
						System.out.println("\tPuffer leer! Worker(" + this.getName() + ") muss warten..");
						bf.wait();
					} else {
						aMsg = bf.obtainMsg();
						System.out.println("Worker(" + this.getName() + ") liest:\n" + aMsg.toString());
						bf.notify();
					}

				}

				count++;
				Thread.sleep(this.getObtainTime());

			} catch (InterruptedException e) {
				System.out.println("Worker (" + this.getName() + ") wurde unterbrochen.");
				break;
			}

		}

		synchronized (bf) {
			while (!bf.isEmpty()) {
				aMsg = bf.obtainMsg();
				System.out.println("Worker(" + this.getName() + ") liest:\n" + aMsg.toString());
			}
			bf.notifyAll();
		}

		System.out.println("Worker(" + this.getName() + ") wird beendet...");
		System.out.println("\t" + this.getName() + " hat " + this.count + " Nachrichten geholt.");

		return true;

	}

	public String getName() {
		return name;
	}

	private long getObtainTime() {
		long d = date.getTime() % 1000;
		return d;
	}

}
