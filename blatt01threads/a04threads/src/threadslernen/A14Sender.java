package threadslernen;

import java.util.Random;
import java.util.concurrent.Callable;


public class A14Sender implements Callable<Boolean> {

	private String name;
	private A14MsgBufferInf bf;
	private int idGen;
	private Random random;

	public A14Sender(String name, A14MsgBufferInf bf) {
		this.name = name;
		this.bf = bf;
		this.idGen = 0;
		this.random = new Random();
	}

	@Override
	public Boolean call() throws Exception {
		System.out.println("\t Worker(" + this.getName() + ") wird gestartet...");

		A14Msg aMsg;
		int randomNum;
		int countMsg =0;
		while (!Thread.interrupted()) {
			randomNum = random.nextInt(30);
			aMsg = this.createMessage(randomNum);
			try {
				Thread.sleep(randomNum * randomNum);
				synchronized (bf) {
					if(bf.isFull()) {
						System.out.println("\tPuffer voll! \t Worker(" + this.getName() + ") muss warten..");
						bf.wait();
					}
					bf.insertMsg(aMsg);
					System.out.println("Worker(" + this.getName() + ") SCHREIBT: \n" + aMsg.toString());
					bf.notify();
				}
				++countMsg;

			} catch (InterruptedException e) {
				System.out.println("Worker(" + this.getName() + ") wurde zum Beenden aufgefordert.");
				break;
			}

		}

		System.out.println("\t Worker(" + this.getName() + ") wird beendet...");
		System.out.println("\t" + this.getName() + " hat " + countMsg + " Nachrichten geschireben.");

		return true;
	}

	private A14Msg createMessage(int num) {
		int id = this.genId();
		A14Msg msg = new A14Msg(id, this.getName(), String.valueOf(num));
		return msg;
	}

	public String getName() {
		return name;
	}

	private int genId() {
		return this.idGen++;
	}

}
