package threadslernen;


public interface A14MsgBufferInf {
	public   void insertMsg(A14Msg msg);
	public  A14Msg obtainMsg();
	public boolean isFull();
	public boolean isEmpty();
	
}
