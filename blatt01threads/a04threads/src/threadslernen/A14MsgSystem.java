package threadslernen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class A14MsgSystem {

	private static long starttime;

	public static void main(String[] args) {
		System.out.println("Diese Lösung wurde erstellt von Oraz Serdarov \n");
		starttime = System.currentTimeMillis();
		System.out.println("Programm A14MsgSystem wird gestartet");
		int buffSize = getBufferSize(args);
		int simTime = getSimTime(args);

		if (buffSize != 0 || simTime != 0) {
			Scanner scanner = new Scanner(System.in);
			String eingabe;
			A14MsgBuffer buff = new A14MsgBuffer(buffSize);
			Collection<Future<Boolean>> results = new ArrayList<Future<Boolean>>();
			Collection<Callable<Boolean>> threadCollection = new ArrayList<Callable<Boolean>>();
			threadCollection.add(new A14Sender("sender1", buff));
			threadCollection.add(new A14Sender("sender2", buff));
			threadCollection.add(new A14Recipient("recipient1", buff));
			threadCollection.add(new A14Recipient("recipient2", buff));
			threadCollection.add(new A14Recipient("recipient3", buff));
			ExecutorService ex = Executors.newCachedThreadPool();
			try {
				results = ex.invokeAll(threadCollection, simTime, TimeUnit.SECONDS);
				ex.shutdown();
				Thread.sleep(2000);


				System.out.println("Puffer vor dem Beenden: \n" + buff.toString());

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			System.out.println("Sie haben einen ungültigen Aufrufparameter übergeben!" + args[0]);

		}

		System.out.println("Programm A14MsgSystem ist beendet.\n Es hat " + (System.currentTimeMillis() - starttime)
				+ " gedauert.");
	}

	private static int getBufferSize(String[] args) {
		int size = 0;

		try {
			size = Integer.parseInt(args[0].trim());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return Math.abs(size);
	}

	private static int getSimTime(String[] args) {
		int size = 0;

		try {
			size = Integer.parseInt(args[1].trim());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return Math.abs(size);
	}

}
