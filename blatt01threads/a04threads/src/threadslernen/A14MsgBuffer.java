package threadslernen;


public class A14MsgBuffer implements A14MsgBufferInf {

	private int nextFee;
	private int SIZE;
	private A14Msg[] messages;
	private int count;
	private int nextFetch;
	private boolean stopInsert;

	public A14MsgBuffer(int size) {
		this.SIZE = size;
		this.stopInsert = false;
		this.nextFee = 0;
		this.nextFetch = 0;
		this.count = 0;
		messages = new A14Msg[this.SIZE];
	}

	public void insertMsg(A14Msg msg) {
		messages[nextFee] = msg;
		this.nextFee = (nextFee + 1) % this.SIZE;
		count++;
	}

	public A14Msg obtainMsg() {
		A14Msg msg = messages[nextFetch];
		messages[nextFetch] = null;
		this.nextFetch = (nextFetch + 1) % this.SIZE;
		count--;
		return msg;

	}

	public String toString() {
		StringBuffer buff = new StringBuffer("");
		for (int i = 0; i < this.SIZE; i++) {
			if (messages[i] != null) {
				buff.append(messages[i] + "\n");
			}
		}
		
		
		if ("".equals(buff.toString())) {
			return "Der Puffer ist leer";
		} else {
			return buff.toString();

		}
	}

	public boolean isFull() {
		return SIZE == count;
	}

	public boolean isEmpty() {
		return count == 0;
	}

}
