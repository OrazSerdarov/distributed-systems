package threadslernen;

public class A14Msg {

	private int id;
	private String sender;
	private String txt;
	
	public A14Msg(int id , String sender , String txt){
		this.id=id;
		this.sender=sender;
		this.txt =txt;
	}
	
	
	public String toString() {
		StringBuffer str  = new StringBuffer();
		str.append("\t\t NachrichtID: " + this.getId() +"\n");
		str.append("\t\t Sender: " + this.getSender()+"\n");
		str.append("\t\t Nachricht: " + this.getTxt()+"\n");
		return str.toString();
	}


	public int getId() {
		return id;
	}


	public String getSender() {
		return sender;
	}


	public String getTxt() {
		return txt;
	}
}
