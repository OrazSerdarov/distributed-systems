Autor: Oraz Serdarov
Verteilte Systeme SS 2020
Blatt 01: Arbeitsumgebung Linux, Threads, Synchronisation
Aufgabe 04
Vorbedingung:
Das aktuelle Arbeitsverzeichnis ist
blatt01threads/a04threads

Generieranleitung:
1. Generieren des ausführbaren Programms mit
javac -d build/classes/ src/threadslernen/*.java

Installationsanleitung:
Entfällt. Die Dateien sind direkt nach Generierung ausführbar.

Bedienungsanleitung:
1a. Start des ausführbaren Programms 
java -cp build/classes threadslernen/A14MsgSystem <pfrsize> <sltime>

Aufruf mit Parameter: <pfrsize> <sltime>
 <pfrsize> - größe des Puffers
 <sltime>  - Simulationszeit

