package threadslernen;

import  java.util.Date;
import java.util.Random;

public class  A13Helper {
  private Random rand;

  public A13Helper() {
      rand = new Random(new Date().getTime());
  }

  public boolean isPrimeNumber(int num) {
  // korrekter, aber schlechter Algorithmus, weil langsam. 
  // Soll aber für die Aufgabe langsam sein!
    if (num < 2) return false;
      for (int i=2; i<num; i++) {
         if (num%i == 0) return false;
      }
      return true;
  } // isPrimeNumber()



} // class
