package threadslernen;

import java.util.Date;
import java.util.Random;

public class A13WorkerThread extends Thread {

	private long cycleTime;

	public A13WorkerThread(String name, long cycleTime) {
		this.setName(name);
		this.cycleTime = cycleTime;
		super.setName(name);
	}

	@Override
	public void run() {
		System.out.println("Worker mit dem Namen '" + this.getName() + "' wird gestartet...");
		A13Helper aHelper = new A13Helper();
		Random rand = new Random(new Date().getTime());
		int cycleNum = 0;
		int num;
		boolean found = false;
		while (true) {
			cycleNum++;
			num = rand.nextInt();
			if (aHelper.isPrimeNumber(num)) {
				System.out.println("  '" + this.getName() + "' hat eine Primzahl gefunden: " + num
						+ " \n  Schleifendurchlaufnummer " + cycleNum);
				found = true;
			}

			try {

				if (Thread.interrupted()) { // clears the flag
					throw new InterruptedException("isPrimeNumber()");
				}
				if (found) {
					Thread.sleep(cycleTime);
					found = false;
				}

			} catch (InterruptedException e) {
				String msg = e.getMessage();
				System.out.print("\nWorker mit dem Namen '" + this.getName() + " wurde während der Methodenausführung von ");
				if (!msg.equals("")) {
					System.out.println(msg + "unterbrochen.\n");
				} else {
					System.out.println("'Thread.sleep()' unterbrochen.\n" );
				}
				return;
			}
		}

	}
}
