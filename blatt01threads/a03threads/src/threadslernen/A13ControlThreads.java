package threadslernen;

public class A13ControlThreads {

	private static long starttime;

	public static void main(String[] args) {
		System.out.println("Diese Lösung wurde erstellt von Oraz Serdarov\n");

		if (args.length != 2) {
			System.out.println("Geben Sie 2 Aufrufparamter bei der Ausführung des Programms! \n"
					+ "Parameter 1 : Zykluswartezeiten für Worker1 \n"
					+ "Parameter 2 : Zykluswartezeiten für Worker2 \n");
			
		} else {
			starttime = System.currentTimeMillis();
			int cycleTimeWorker1 = Integer.valueOf(args[0]);
			int cycleTimeWorker2 = Integer.valueOf(args[1]);
			
			Thread worker1 = new A13WorkerThread("worker1", cycleTimeWorker1);
			Thread worker2 = new Thread(new A13WorkerRunnable("worker2", cycleTimeWorker2));
			worker1.start();
			worker2.start();
			try {
				Thread.sleep(3000);
				worker1.interrupt();
				Thread.sleep(1000);
				worker2.interrupt();

				worker1.join();
				worker2.join();
			} catch (InterruptedException e) {
				e.printStackTrace();

			}

			System.out.println("------------------------------------");
			System.out.println("A13ControlThreads beendet sich... \n\n"
					+ "Es hat " + (System.currentTimeMillis() - starttime) + " Millisekunden gedauert.");

		}
	}
}
