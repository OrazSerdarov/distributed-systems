Autor: Oraz Serdarov
Verteilte Systeme SS 2020
Blatt 01: Arbeitsumgebung Linux, Threads, Synchronisation
Aufgabe 03
Vorbedingung:
Das aktuelle Arbeitsverzeichnis ist
blatt01threads/a03threads

Generieranleitung:
1. Generieren des ausführbaren Programms mit
javac -d build/classes/ src/threadslernen/*.java

Installationsanleitung:
Entfällt. Die Dateien sind direkt nach Generierung ausführbar.

Bedienungsanleitung:
1a. Start des ausführbaren Programms 
java -cp build/classes threadslernen/A13ControlThreads <c1time> <c2time>

Aufruf mit Parameter: <c1time> <c2time>
<c1time> bzw. <c2time> ist Wartezeit eines Threads, nachdem er eine Primzzahl gefunden hat.
Angaben für <c1time> und <c2time> in Millisekunden

