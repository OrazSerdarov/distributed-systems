Autor: Oraz Serdarov
Verteilte Systeme SS 2020
Blatt 01: Arbeitsumgebung Linux, Threads, Synchronisation
Aufgabe 01
Vorbedingung:
Das aktuelle Arbeitsverzeichnis ist
blatt01threads/a01threads

Generieranleitung:
1. Generieren des ausführbaren Programms mit
javac -d build/classes/ src/threadslernen/A11HelloWorld.java


Installationsanleitung:
Entfällt. Die Dateien sind direkt nach Generierung ausführbar.

Bedienungsanleitung:
1. Start des ausführbaren Programms mit
java -cp build/classes threadslernen/A11HelloWorld 

Optionalkönnen Parameter übergeben werden.