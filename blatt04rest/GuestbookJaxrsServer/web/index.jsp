

<!DOCTYPE html>
<% 
String acceptStr=request.getHeader("Accept");
if (acceptStr.contains("text/xml")){
 // return XML-Format
 response.setHeader("Content-Type", "text/xml");
%>
<jsp:forward page="index.xml" />
<% } // if
// else return HTML-Format
%>
<% String pathToResource = request.getRequestURL() + "webresources";
  String contextpath = request.getContextPath();
  char first = contextpath.charAt(0);
  if (first == '/') {
    contextpath = contextpath.substring(1);
  }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title><%=contextpath%>: REST-based Webservice, Index page</title>
  <style type="text/css">
   .code {color:blue; font-family: monospace}
   table {border-style: solid; width: 1px;}
  </style>
  
 </head>
 <body>
  <h3><%=contextpath%>: a REST-based Webservice</h3>
  <p><%=contextpath%> is a REST-based Webservice which is implemented by the project also called
   <%=contextpath%> developed using Netbeans 7.2.
  </p>
  <p>The general URL of the webservice resources is<br /> 
   <span class="code"><%=request.getRequestURL()%>webresources/{servicepath}/{Resource-URI}</span></p>

  <p>
   The URL of the specification of the webservice is <br />
   <a href="<%=pathToResource%>/help"><span class="code"><%=pathToResource%>/help</span></a></p>
  <p>The WADL-File of the webservcie can be retrieved at<br />
   <% String pathToWadl = request.getRequestURL() + "webresources/application.wadl";%>
   <a href="<%=pathToWadl%>"><span class="code"><%=pathToWadl%></span></a>
  </p> 
  <p>        The URL of the service description of the resource <span style="font-weight:bold">guestbook</span> is <br />
   <a href="<%=pathToResource%>/guestbook"><span class="code"><%=pathToResource%>/guestbook</span></a></p>
 
  <p>        The URL of the service description of the webservice is <br />
   <a href="<%=request.getRequestURL()%>index.xml"><span class="code"><%=request.getRequestURL()%></a> 
   used in a request with the HTTP-Header Accept=&quot;text/xml&quot;</span> <br>addressing the file index.xml.</p>
 
  <h4>A Web Application Client for <%=contextpath%> is:</h4>
  <p><a href="/GuestbookJaxrsClient">Guestbook</a></p>

 </body>
</html>
