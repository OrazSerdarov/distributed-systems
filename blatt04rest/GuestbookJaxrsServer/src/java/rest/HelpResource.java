package rest;

import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

@Path("help")
public class HelpResource {
    
    static final private String CRLF = System.getProperty("line.separator");

    /**
     * Creates a new instance of HelpResource
     */
    public HelpResource() {
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getHtml() {
        String res;
        res = "<html><head><title>GuestbookJaxrs</title>" + CRLF;
        res = res + "<style type='text/css'>" + CRLF;
        res = res + ".code {color:blue;font-family:monospace;} " + CRLF;

        res = res + "table {border-style: solid; }" + CRLF;
        res = res + "tr {border-style: solid; border-width: 1px;}" + CRLF;
        res = res + "td {border-style: solid; border-width: 1px;}" + CRLF;

        res = res + "</style>" + CRLF;
        res = res + "</head><body>" + CRLF;
        res = res + "<h3>GuestbookJaxrs: specification of resources and operations</h3>" + CRLF;

        // Vector<String[]> v = new Vector<String[]>();
        String tableheader[]
                = {"NB", "URI-Templates", "Verb", "Request Type", "Response Type", "Description"};
        String tableelements[][] = {
    //      {"/", "GET", "&nbsp;", "text/html", "Entrypoint human readable"},
    //      {"/", "GET", "&nbsp;", "text/xml", "Entrypoint maschine readable"},
          {"/guestbook", "GET", "&nbsp;", "text/xml", "Service description of resource guestbook"},
          {"/guestbook/entries", "GET", "&nbsp;", "text/xml", "Read the list of current entries"},
          {"/guestbook/entries", "POST", "application/x-www-form-urlencoded<hr />name={val}&amp;msg={val}", "text/xml", "Create a new entry"},
          {"/guestbook/entries/{id}", "GET", "&nbsp;", "text/plain", "Read entry with the Id=id"},
          {"/guestbook/entries/{id}", "GET", "&nbsp;", "text/xml", "Read entry with the Id=id"},
          {"/guestbook/entries/{id}", "DELETE", "&nbsp;", "text/xml", "Delete entry with the Id=id"},
          {"/guestbook/entries/{id}", "PUT", "application/x-www-form-urlencoded<hr />name={val}&amp;msg={val}", "text/xml", "Modify entry with the Id=id"},
          {"/guestbook/entries/search<br />?name={val}", "GET", "&nbsp;", "text/plain", "Read entries with the name=val"},
          {"/guestbook/entries/search<br />?name={val}", "GET", "&nbsp;", "text/xml", "Read entries with the name=val"},
          {"/help", "GET", "&nbsp;", "text/html", "Human readable specification of webservice GuestbookJaxrs"}
        };

        res = res + "<table>" + CRLF;
        // construct header line
        res = res + "<tr>";
        for (int i = 0; i < tableheader.length - 1; i++) {
          res = res + "<td>" + tableheader[i] + "</td>";
        }
        res = res + "<td style='width:300px'>" + tableheader[tableheader.length - 1] + "</td>";
        // construct normal lines
        res = res + "</tr>";
        for (int i = 0; i < tableelements.length; i++) {
          res = res + "<tr>";
          res = res + "<td>" + (i+1) + "</td>";

          for (int j = 0; j < tableelements[i].length; j++) {
            res = res + "<td>" + tableelements[i][j] + "</td>";
          }
          res = res + "</tr>";
        }

        res = res + "</table>";

        res = res + "<p>In the default configuration of the Webservice in Netbeans 8.2 the resources are available in Glassfish 4.1.1/Tomcat 8 at:<br />" + CRLF;
        res = res + "<span style='font-family:monospace; color:blue'>http://&lt;domain:port&gt;/GuestbookJaxrs/webresources/&lt;URI-Template&gt;</span></p>" + CRLF;
        res = res + "</p>";
        res = res + "<p>";
        res = res + "The human readable entry point is <span style='font-family:monospace; color:blue'>http://&lt;domain:port&gt;/GuestbookJaxrs/</span> with HTTP-Method GET and Content-Type text/html<br>";
        res = res + "The machine readable entry point is <span style='font-family:monospace; color:blue'>http://&lt;domain:port&gt;/GuestbookJaxrs/</span> with HTTP-Method GET and Content-Type text/xml<br>";
        res = res + "</p>";

        res = res + "</body></html>";
        return res;
    }
}
