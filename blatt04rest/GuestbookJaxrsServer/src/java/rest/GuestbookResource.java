package rest;

import model.GuestbookEntries;
import model.GuestbookEntry;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Response;


@Path("guestbook")
public class GuestbookResource {

    @Context
    private UriInfo context;
    @Context
    private HttpServletRequest request;
    
    static final private String CRLF = System.getProperty("line.separator");

    /**
     * Creates a new instance of GuestbookResource
     */
    public GuestbookResource() {
    }

    @GET
    @Produces(javax.ws.rs.core.MediaType.TEXT_XML)
    public String getXml() {
        String str = "<?xml version=\"1.0\"?>" + CRLF;
        str += "<servicedescription>" + CRLF;
        str += "<link rel='entries' href='entries' />" + CRLF;
        str += "</servicedescription>" + CRLF;
        return str;        
    }

    @GET
    @Path("/entries")
    @Produces(javax.ws.rs.core.MediaType.TEXT_XML)
    public Response getAllEntries() {
        try {
            GuestbookEntries book = new GuestbookEntries(request.getServletContext().getRealPath("/WEB-INF"));
            String xml = book.getEntriesAsXml();
            return Response.ok(xml).build();
        } catch(IOException e) {
            throw new InternalServerErrorException(e.getMessage());
        }        
    }
    
    @POST
    @Path("/entries")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(javax.ws.rs.core.MediaType.TEXT_XML)
    public Response createEntry(@FormParam("name") String name, @FormParam("msg") String msg) {
        if(name == null || msg == null) {
            throw new BadRequestException();
        }
        try {
            GuestbookEntries book = new GuestbookEntries(request.getServletContext().getRealPath("/WEB-INF"));
            GuestbookEntry newEntry = book.create(name, msg);
            String location = context.getAbsolutePath() + String.valueOf(newEntry.getId());
            String xml = "<?xml version=\"1.0\" ?>" + newEntry.XMLEncodeEntry();
            return Response.status(Response.Status.CREATED).location(URI.create(location)).entity(xml).build();
        } catch(IOException e)  {
            throw new InternalServerErrorException(e.getMessage());
        }      
        
    }
    
    @GET
    @Path("/entries/{id}")
    @Produces(javax.ws.rs.core.MediaType.TEXT_XML)
    public Response getEntryXmlById(@PathParam("id") int id) {
        try {
            GuestbookEntries book = new GuestbookEntries(request.getServletContext().getRealPath("/WEB-INF"));
            String xml = book.getEntryXML(id);
            if(xml == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("Entry with id " + id + " could not be found!").build();
            }
            return Response.ok(xml).build();
        } catch(IOException e) {
            throw new InternalServerErrorException(e.getMessage());
        }        
    }
    
    @GET
    @Path("/entries/{id}")
    @Produces(javax.ws.rs.core.MediaType.TEXT_PLAIN)
    public Response getEntryPlainById(@PathParam("id") int id) {
        try {
            GuestbookEntries book = new GuestbookEntries(request.getServletContext().getRealPath("/WEB-INF"));
            String text = book.getEntryPlain(id);
            if(text == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("Entry with id " + id + " could not be found!").build();
            }
            return Response.ok(text).build();
        } catch(IOException e) {
            throw new InternalServerErrorException(e.getMessage());
        }        
    }
    
    @DELETE
    @Path("/entries/{id}")
    @Produces(javax.ws.rs.core.MediaType.TEXT_XML)
    public Response deleteEntryById(@PathParam("id") Integer id) {
        if(id == null)
            throw new BadRequestException();
        try {
            GuestbookEntries book = new GuestbookEntries(request.getServletContext().getRealPath("/WEB-INF"));
            String xml = book.deleteEntry(id);
            if(xml == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("Entry with id " + id + " could not be found!").build();
            }
            return Response.status(Response.Status.NO_CONTENT).build(); 
        } catch(IOException e) {
            throw new InternalServerErrorException(e.getMessage());
        }        
    }
    
    @PUT
    @Path("/entries/{id}")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(javax.ws.rs.core.MediaType.TEXT_XML)
    public Response modifyEntryById(@PathParam("id") Integer id, @FormParam("name") String name, @FormParam("msg") String msg) {
        if(id == null || name == null || msg == null) {
            throw new BadRequestException();
        }
        try {
            GuestbookEntries book = new GuestbookEntries(request.getServletContext().getRealPath("/WEB-INF"));
            String xml = book.putEntry(id, name, msg);
            if(xml == null) {
                GuestbookEntry newEntry = book.create(name, msg);           
                String location = "";
                List<PathSegment> segments = context.getPathSegments();
                for(int i=0; i<segments.size()-1; i++)
                    location += segments.get(i) + "/";
                location += newEntry.getId();
                xml = "<?xml version=\"1.0\" ?>" + newEntry.XMLEncodeEntry();
                return Response.status(Response.Status.CREATED).location(URI.create(location)).entity(xml).build();
            } else {
                return Response.ok(xml).build();
            }
        } catch(IOException e)  {
            throw new InternalServerErrorException(e.getMessage());
        }      
        
    }
    
    @GET
    @Path("/entries/search")
    @Produces(javax.ws.rs.core.MediaType.TEXT_PLAIN)
    public Response searchEntriesPlainTextByName(@QueryParam("name") String name) {
        if(name == null) {
            throw new BadRequestException();
        }
        try {
            GuestbookEntries book = new GuestbookEntries(request.getServletContext().getRealPath("/WEB-INF"));
            String result = book.searchEntries(name, "text/plain");            
            return Response.ok(result).build();
        } catch(IOException e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }
    
    @GET
    @Path("/entries/search")
    @Produces(javax.ws.rs.core.MediaType.TEXT_XML)
    public Response searchEntriesXmlByName(@QueryParam("name") String name) {
        if(name == null) {
            throw new BadRequestException();
        }
        try {
            GuestbookEntries book = new GuestbookEntries(request.getServletContext().getRealPath("/WEB-INF"));
            String result = book.searchEntries(name, "text/xml");
            return Response.ok(result).build();
        } catch(IOException e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }
}
