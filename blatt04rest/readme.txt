Autor: Oraz Serdarov
Verteilte Systeme SS 2020
Blatt 04: REST-basierte Webservices

Vorbedingungen:
Der Rechner verfügt über NetBeans IDE und Tomcat (Tomcat ist gestartet und läuft auf Port 8080)

Deploymentanleitung (funktioniert für beide Projekte GuestbookJaxrsServer und GuestbookJaxrsClient glech)
  1. Projekt in NetBeans öffnen (In NetBeans oben links File -> Open Project -> zum Verzeichnis GuestbookJaxrsServer bzw. GuestbookJaxrsClient gehen)
  2. Projekt erstellen (In der linken Leiste mit Rechtsklick auf das Projekt klicken und "Clean and Build" auswählen)
  3. Projekt starten (In der linken Leiste mit Rechtsklick auf das Projekt klicken und "run" auswählen)

Der Server ist unter der URL http://10.8.13.15:8080/GuestbookJaxrsServer erreichbar.
Der Client ist unter der URL http://10.8.13.15:8080/GuestbookJaxrsClient erreichbar.

Statt IP-Adresse kann man auch localhost angeben.
 
