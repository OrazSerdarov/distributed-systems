package socketslernen;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Scanner;

import java.io.DataInputStream;

public class TcpCalculationClient {

	private Scanner sc;
	private SocketAddress addr;
	private FolderRequest request = null;
	private FolderResponse response = null;
	private boolean beenden;

	public TcpCalculationClient(String args[]) {
		addr = new InetSocketAddress(args[0], new Integer(args[1]));
		sc = new Scanner(System.in);
		beenden=false;
	}

	public static void main(String args[]) {
		System.out.println("Diese Lösung wurde erstellt von Oraz Serdarov\n");

		if (isInputOk(args)) {

			TcpCalculationClient tcc = new TcpCalculationClient(args);

			System.out.println("TcpCalculationClient gestartet.");
			System.out.println(" Die Verbindung zum Server=" + args[0] + " an Port=" + args[1] + "wird hergestellt....");

			try (Socket socket = new Socket()) {

				socket.connect(tcc.addr, 3000);
				System.out.println("   Die Verbindung zum Server " + args[0] + " an Port=" + args[1] + "ist hergestellt");
				tcc.exec(socket);
			} catch (IOException e) {
				System.out.println("Keine Verbindung konnte hergestellt werden! \n" + e.getMessage());
			}

		} else {
			System.out.println("Geben Sie ein 'Hostname' und eine 'Portnummer' als Aufrufparameter !");
		}
		
		System.out.println("Client beendet sich jetzt...\n");

	}

	private void exec(Socket socket) throws IOException {

		System.out.println("'quit' um das Clientprogramm zu beenden.");
		System.out.println("'1' für die Server-Funktion 'Gesamtsumme der Ganzzahlen'.");
		System.out.println("'2' für die Server-Funktion 'Anzahl der positiven Ganzzahlen'.");
		System.out.println("'3' um die Verbindung zum Server zu schließen.");
		System.out.println("'4' um den Server herunterzufahren.");
		prepareUserRequest();
		while (!beenden) {
		
			
			DataOutputStream outputData = new DataOutputStream(socket.getOutputStream());
			outputData.writeByte(request.getMethodId());
			outputData.writeShort(request.getRequestSize());
			outputData.write(request.getByteArr(), 0, request.getByteArr().length);
			outputData.flush();

			
			System.out.println("Antwort des Servers : \n\n");
		
			DataInputStream inputData= new DataInputStream(socket.getInputStream());
			response= new FolderResponse();
			response.setResponseType(inputData.readByte());
			response.setResponse(inputData.readInt());
				
			if(response.getResponseType()==1) {
				System.out.println("Die Summe its: " + response.getResponse());
			}else if(response.getResponseType()==2) {
				System.out.println("Die Anzahl der positiven Zahlen ist: " + response.getResponse());
			}else if(response.getResponseType()==3) {
				System.out.println("Die Verbindung zum Server wurde beendet. Status" + response.getResponse());
				break;
			}else if(response.getResponseType()==4) {
				System.out.println("Server wird heruntergefahren. Status" + response.getResponse());
			}else{
				System.out.println("Der Server hat den Request nicht verstanden! Responsestatus: " +response.getResponseType()
				+ " Inhalt:" + response.getResponse());
			}
			
			prepareUserRequest();
		}

	}

	private void prepareUserRequest() {
		this.request = new FolderRequest();
		boolean inputOk = false;
		while (!inputOk) {
			inputOk = true;
			System.out.print("Ihre Eingabe:");
			String input = sc.next();
			if ("quit".equalsIgnoreCase(input.trim())) {
				beenden=true;
			} else {
				int funcNum = getFuncNum(input);
				switch (funcNum) {
				case 1:
					request.setMethodId((byte) 1);
					fillData();
					break;
				case 2:
					request.setMethodId((byte) 2);
					fillData();
					break;

				case 3:
					String s = "herunterfahren";
					request.setMethodId((byte) 3);
					request.setRequestSize((short) s.length());
					request.setByteArr(s.getBytes());
					break;
				case 4:
					String ss = "schluss";
					request.setMethodId((byte) 3);
					request.setRequestSize((short) ss.length());
					request.setByteArr(ss.getBytes());
					
				default:
					System.out.println("Diese Funktionsnummer" + funcNum + " existiert nicht!\nGeben Sie 1,2,3 oder quit ein!");
					inputOk = false;
					break;
				}

			}

		}

	}

	private void fillData() {
		System.out.println("Geben Sie die Anzahl der Zahlen, die Sie eingeben möchten.");
		System.out.print("Ihre Eingabe:");
		short arrLength = sc.nextShort();
		request.setRequestSize(arrLength);
		ByteBuffer bb = ByteBuffer.allocate(arrLength * 4);
		for (int i = 0, index = 0; i < arrLength; i++, index = index + 4) {
			try {
				System.out.print((i + 1) + ". Zahl eingeben:");
				int num = sc.nextInt();
				System.out.println((i + 1) + ". eingelesen " + num);
				bb.putInt(index, num);
			} catch (Exception e) {
				System.out.println("Etwas ist schifgelaufen!");
			}

		}
		bb.rewind();
		request.setByteArr(bb.array());

	}

	private int getFuncNum(String input) {
		int ret = -1;
		try {
			ret = Integer.parseInt(input);
		} catch (NumberFormatException e) {
			System.out.println("Falsche Eingaben!" + e.getMessage());
		}
		return ret;
	}

	private static boolean isInputOk(String args[]) {
		boolean b1 = args.length == 2;
		boolean b2 = args[0].length() > 0;
		boolean b3 = isNumeric(args[1]);

		return b1 && b2 && b3;
	}

	private static boolean isNumeric(String str) {
		return str != null && str.matches("[-+]?\\d*\\.?\\d+");
	}

	

}
