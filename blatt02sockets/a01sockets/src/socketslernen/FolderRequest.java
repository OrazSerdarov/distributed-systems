package socketslernen;

public class FolderRequest {
	
	private byte methodId;
	private short requestSize;
	private byte[]  byteArr ;

	
	public byte getMethodId() {
		return methodId;
	}
	public void setMethodId(byte methodId) {
		this.methodId = methodId;
	}
	public short getRequestSize() {
		return requestSize;
	}
	public void setRequestSize(short requestSize) {
		this.requestSize = requestSize;
	}
	public byte[] getByteArr() {
		return byteArr;
	}
	public void setByteArr(byte[] byteArr) {
		this.byteArr = byteArr;
	}



}
