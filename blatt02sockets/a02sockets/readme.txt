Autor: Oraz Serdarov
Verteilte Systeme SS 2020
Blatt 02: Socketprogrammierung
Aufgabe 01
Vorbedingung:
Das aktuelle Arbeitsverzeichnis ist


Generieranleitung:
1. Generieren des ausführbaren Programms mit
javac -d build/classes/ src/socketslernen/*.java

Installationsanleitung:
Entfällt. Die Dateien sind direkt nach Generierung ausführbar.

Bedienungsanleitung:
1a. Start des Serverprogramms 
java -cp build/classes socketslernen/CalcTcpServer <port> (z.B 10510)


2. Start des Clients (evt. mehreren) 
java -cp build/classes socketslernen/TcpCalculationClient <hostname> <port> (z.B localhost 10510)