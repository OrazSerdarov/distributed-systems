package socketslernen;

public class FolderResponse {
	private byte responseType;
	private int response;
	
	
	public int getResponse() {
		return response;
	}
	public void setResponse(int response) {
		this.response = response;
	}
	public byte getResponseType() {
		return responseType;
	}
	public void setResponseType(byte responseType) {
		this.responseType = responseType;
	}



}
