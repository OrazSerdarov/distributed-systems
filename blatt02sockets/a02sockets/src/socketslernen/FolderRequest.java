package socketslernen;

public class FolderRequest {
	
	private byte methodId;
	private short requestSize;
	private int simTime ;
	private byte[]  byteArr ;
	

	
	public int getSimTime() {
		return simTime;
	}
	public void setSimTime(int simTime) {
		this.simTime = simTime;
	}
	public byte getMethodId() {
		return methodId;
	}
	public void setMethodId(byte methodId) {
		this.methodId = methodId;
	}
	public short getRequestSize() {
		return requestSize;
	}
	public void setRequestSize(short requestSize) {
		this.requestSize = requestSize;
	}
	public byte[] getByteArr() {
		return byteArr;
	}
	public void setByteArr(byte[] byteArr) {
		this.byteArr = byteArr;
	}



}
