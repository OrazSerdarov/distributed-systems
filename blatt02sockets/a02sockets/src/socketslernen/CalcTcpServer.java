package socketslernen;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class CalcTcpServer {

	private boolean beenden; // für parallen Server

	public CalcTcpServer() {
		this.beenden = false;
	}

	public void serverBeenden() {
		beenden = true;
	}

	public static void main(String args[]) {
		System.out.println("Diese Lösung wurde erstellt von Oraz Serdarov\n");

		if (isInputOk(args)) {
			System.out.println("Rechen-TCP-Server (multi-threaded) ist gestartet.");
			System.out.println("  Aktuelle Einstellungen:\n   port: " + args[0] + "\n");
			CalcTcpServer cts = new CalcTcpServer();
			try (ServerSocket socket = new ServerSocket(new Integer(args[0]))) {

				while (!cts.beenden) {
					Socket client = null;
					client = socket.accept();
					System.out.println(" Ein Client hat eine Verbindung hergestellt.");
					Thread tcpWorker = cts.createWorker(client);
					try {
						tcpWorker.start();
						tcpWorker.join();
					} catch (Exception e) {
						System.out.println("Fehler beim WorkerThread");
					}
				}
				System.out.println("Server wurde zum Herunterfahren veranlasst. \n Server beendet sich...");

			} catch (IOException e) {
				System.out.println("Keine Verbindung konnte hergestellt werden! \n" + e.getMessage());
			}

		} else {

			System.out.println("Geben Sie eine 'Portnummer' als Aufrufparameter !");

		}

	}

	private ParlCalcTcpWorker createWorker(Socket client) {
		return new ParlCalcTcpWorker(client);
	}

	private static boolean isInputOk(String args[]) {
		boolean b1 = args.length == 1;
		boolean b2 = isNumeric(args[0]);
		return b1 && b2;
	}

	private static boolean isNumeric(String str) {
		return str != null && str.matches("[-+]?\\d*\\.?\\d+");
	}

	private class ParlCalcTcpWorker extends Thread {
		private Socket client = null;
		private DataInputStream inputStream = null;
		private DataOutputStream outputStream = null;
		private FolderResponse response;

		public ParlCalcTcpWorker(Socket client) {
			this.client = client;

			try {
				this.inputStream = new DataInputStream(client.getInputStream());
				this.outputStream = new DataOutputStream(client.getOutputStream());
			} catch (IOException e) {
				System.out.println("Worker " + this.getName() + " konnte keine In/OutputStreams holen!");

			}
		}

		public void run() {
			System.out.println("Worker " + this.getName() + " wurde erstellt");
			boolean breakConnection = false;

			while (!breakConnection) {
				try {
					byte methodId = inputStream.readByte();
					short requestSize = inputStream.readShort();
					int simTime = inputStream.readInt();
					String txt = "";
					response = new FolderResponse();
					System.out.println("Worker " + this.getName() + " hat Request empfangen.");
					System.out.println("MethodId: " + methodId);
					System.out.println("Num of Elements of the request: " + requestSize);
					switch (methodId) {
					case 1:
						response.setResponseType((byte) 1);
						response.setResponse(calcSum(getDataInt(requestSize), simTime));
						break;
					case 2:
						response.setResponseType((byte) 2);
						response.setResponse(countPosNumbers(getDataInt(requestSize), simTime));
						break;
					case 3:
						txt = getDataString(requestSize);
						System.out.println("String is:" + txt);
						if ("schluss".equalsIgnoreCase(txt)) {
							response.setResponseType((byte) 3);
							breakConnection = true;
							Thread.sleep(simTime);
						}
						break;
					case 4:
						txt = getDataString(requestSize);
						System.out.println("String is:" + txt);
						if ("herunterfahren".equalsIgnoreCase(txt)) {
							response.setResponseType((byte) 4);
							System.out.println("Worker fordert den Server zum beenden.");
							breakConnection = true;
							Thread.sleep(simTime);
							serverBeenden();
						}
						break;

					}
					doResponse();
					if (breakConnection == true && client != null) {
						try {
							client.close();
						} catch (IOException e) {
							System.out.println("Client konnte nicht geschlossen werden!");

						}
					}

				} catch (IOException e) {
					System.out.println("Worker IOException" + e.getMessage());
					breakConnection = true;
					if (client != null) {
						try {
							client.close();
						} catch (IOException ee) {
							System.out.println("Client konnte nicht geschlossen werden!" + ee.getMessage());

						}
					}
				}catch(InterruptedException ie) {
					System.out.println("Thread interrupted!" + ie.getMessage());
				}

			}
		

			System.out.println("Worker " + this.getName() + " beendet sich jetzt...");
		}

		private void doResponse() throws IOException {
			outputStream.writeByte(response.getResponseType());
			outputStream.writeInt(response.getResponse());
			outputStream.flush();
			System.out.println("Response ist abgeschickt!");
		}

		private int calcSum(int[] data, int simTime) {
			int sum = 0;
			for (int i = 0; i < data.length; i++) {
				sum += data[i];
			}
			try {
				Thread.sleep(simTime);
			} catch (InterruptedException e) {
				System.out.println("Thead" + this.getName() + " is interrupted while sleeping :" + e.getMessage());
			}
			return sum;
		}

		private int countPosNumbers(int[] data, int simTime) {
			int ret = 0;
			for (int i = 0; i < data.length; i++) {
				if (data[i] > 0) {
					ret++;
				}
			}
			try {
				Thread.sleep(simTime);
			} catch (InterruptedException e) {
				System.out.println("Thead" + this.getName() + " is interrupted while sleeping :" + e.getMessage());
			}

			return ret;
		}

		private String getDataString(short requestSize) throws IOException {
			byte[] arr = new byte[requestSize];
			System.out.println("Requestsize " + requestSize);
			for (int i = 0; i < requestSize; i++) {
				arr[i] = inputStream.readByte();
			}
			return new String(arr);
		}

		private int[] getDataInt(short requestSize) throws IOException {
			int data[] = new int[requestSize];
			for (int i = 0; i < requestSize; i++) {
				data[i] = inputStream.readInt();
			}
			return data;
		}

	}

}
