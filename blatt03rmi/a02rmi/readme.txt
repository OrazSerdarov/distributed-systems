Autor: Oraz Serdarov
Verteilte Systeme SS 2020
Blatt 03: Java-RMI-Programmierung
Aufgabe 02
Vorbedingung:
Das aktuelle Arbeitsverzeichnis ist
blatt03rmi/a02rmi

Generieranleitung:
1. Generieren des ausführbaren Programms mit
javac -d build/classes/ src/rmilernen/tokenfibon/*.java

Installationsanleitung:
Entfällt. Die Dateien sind direkt nach Generierung ausführbar.

Bedienungsanleitung:

1. Start des des Servers
java -cp build/classes/ rmilernen/tokenfibon/A32RmiServer

2. Start des des Clients
Aufruf mit Parameter: 
java -cp build/classes/ rmilernen/tokenfibon/A32RmiClient





