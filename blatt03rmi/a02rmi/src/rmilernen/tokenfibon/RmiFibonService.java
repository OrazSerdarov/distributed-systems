package rmilernen.tokenfibon;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RmiFibonService extends Remote {

	public long rmiFibon(int i) throws RemoteException;

	public Long[] isrmiFibonArray(int i) throws RemoteException;

	public EinfachVerketteteListe rmiConvertToList(Long[] arr) throws RemoteException;

	public void rmiShutdown() throws RemoteException;
}
