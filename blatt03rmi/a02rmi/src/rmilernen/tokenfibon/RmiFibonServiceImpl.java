package rmilernen.tokenfibon;

import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class RmiFibonServiceImpl implements RmiFibonService {

	private String name;

	public RmiFibonServiceImpl(String name) throws RemoteException {
		this.name = name;
	}

	@Override
	public long rmiFibon(int i) throws RemoteException {
		System.out.println("Client ruft: rmiFibon()");

		if (i < 0) {
			return 0;
		} else {
			return this.fibonacci(i + 1);
		}
	}

	@Override
	public Long[] isrmiFibonArray(int i) throws RemoteException {
		System.out.println("Client ruft: isrmiFibonArray()");
	
		if (i < 1) {
			return new Long[1];
		} else {
		
			int lastFibNum = i + 1;
			Long ret[] = new Long[numOfFib(lastFibNum)];			
			int j = 1;
			int a =0;
			long fib = fibonacci(j);
			while( fib <= lastFibNum) {
				
				if(!contains(ret, fib) || j==1 ||j==2 ) {
					ret[a++] = new Long(fib);
						
				}
				fib =fibonacci(j++);
			}
			return ret;
		}

	}
	
	
	private int numOfFib(long fibNum) {
	int ret =0;
	int i =1;
	long fib =fibonacci(i);
	long tempFib ;
		while(fibNum >= fib ) {
			tempFib=fib;
			fib =fibonacci(++i);
			if(tempFib!=fib || fib ==1) {
				ret++;
			}
		}
		return ret;
		
		
	}
	private boolean contains(Long [] arr , long i) {
		for(int j=0 ;j<arr.length;j++) {
			if(arr[j]==null) {
				break;
			}else if(arr[j].longValue()==i){
				return true;
			}
			
		}
		return false;
	}

	@Override
	public EinfachVerketteteListe rmiConvertToList(Long[] arr) throws RemoteException {
		System.out.println("Client ruft: rmiConvertToList()");

		if (arr.length <= 0) {
			return null;
		} else {
			
			EinfachVerketteteListe evl = new EinfachVerketteteListe();
			for (int i = 0; i < arr.length; i++) {
				evl.addListElement(new ListElement(arr[i], i));
			}
			
			
			
			return evl;
		}

	}

	@Override
	public void rmiShutdown() throws RemoteException {
		System.out.println("Client ruft: rmiShutdown()");

		try {
			UnicastRemoteObject.unexportObject((Remote) this, false);
			Naming.unbind(this.name);

			Registry registry = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
			String[] nams =registry.list();
			nams = registry.list();
			System.out.print("Liste der registrierte Remote-Objecte vor dem Beenden:");

			if (nams.length > 0) {
				System.out.print("[ ");
				for (int i = 0; i < nams.length; i++) {
					System.out.print(" " + nams[i] + ", ");
				}
				System.out.println(" ] ");
			} else {
				System.out.println(" keine ! ");
			}
			System.exit(1);
		} catch (Exception e) {
			System.out.println("Exception is thrown while trying to execute shut down" + e.getStackTrace());
		}

	}

	public String getName() {
		return name;
	}

	
	private long fibonacci(int a) {

		if (a == 1 || a == 2)
			return 1;

		else
			return fibonacci(a - 1) + fibonacci(a - 2);

	}
}
