package rmilernen.tokenfibon;

import java.rmi.Naming;
import java.rmi.Remote;
import java.util.Scanner;


public class A32RmiClient {
	public static void main(String args[]) {
		System.out.println("Diese Lösung wurde erstellt von Oraz Serdarov\n");
		System.out.println("A32RmiClient wird gestartet...");

		String remoteObjName = "rmi://localhost/" + RmiFibonService.class.getSimpleName(); // temp
		int fibonI = 6;
		Long[] arr = { new Long(57), new Long(60), new Long(11), new Long(49) };
		if (args.length > 0) {
			fibonI = Integer.valueOf(args[0]);
		}

		System.out.println("Ziel der RMI-Aufrufe ist: " + remoteObjName + "\n");

		try (Scanner sc = new Scanner(System.in)) {
			RmiFibonService fibonService = (RmiFibonService) Naming.lookup(remoteObjName);

			System.out.println("rmiFibon(" + fibonI + "). Ergebnis: " + fibonService.rmiFibon(fibonI));

			System.out.println("isrmiFibonArray(" + fibonI + "). Ergebnis: ");
			arr = fibonService.isrmiFibonArray(fibonI);
			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i] + " ");
			}

			System.out.println("\nrmiConvertToList(arr). Ergebnis: \n" + fibonService.rmiConvertToList(arr).toString());

			System.out.println("Geben Sie die Zeichenkette 'quit' um den Server zu beenden:");
			String input = sc.nextLine();
			if ("quit".equalsIgnoreCase(input.trim())) {
				System.out.println("rmiShutdown().");
				fibonService.rmiShutdown();
			}

		} catch (Exception e) {
			System.out.println("Exception is occured while executing Remote-Method: \n" + e.getMessage());
		}
	}
}
