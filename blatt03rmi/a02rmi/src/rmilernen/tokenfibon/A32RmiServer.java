package rmilernen.tokenfibon;

import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;



public class A32RmiServer {

	public static void main(String args[]) {
		System.out.println("Diese Lösung wurde erstellt von Oraz Serdarov\n");
		System.out.println("A32RmiServer wird gestartet...");
		String input;

		String remoteObjName = RmiFibonService.class.getSimpleName();
		System.out.println("Aktuelles Arbeitsverzeichnis ist :" + System.getProperty("user.dir"));  //temp
		try(Scanner sc = new Scanner(System.in)) {
			// temp
			LocateRegistry.createRegistry(Registry.REGISTRY_PORT);

			
			RmiFibonServiceImpl fibonService = new RmiFibonServiceImpl(remoteObjName);
			RmiFibonService stub = (RmiFibonService) UnicastRemoteObject.exportObject(fibonService, 0);
			
			// RemoteServer.setLog(System.out);
			Registry registry = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
			registry.rebind(fibonService.getName(), stub);
			System.out.println("Remote-Objekt("+ fibonService.getName()+ ") wurde beim Naming-Service regestriet!");

	
			System.out.println("Geben Sie die Zeichenkette 'quit' um den Server zu beenden:");
			input = sc.nextLine();
			String nams[] = registry.list();
			
			if ("quit".equalsIgnoreCase(input)) {
				nams = registry.list();
				if (nams.length == 0) {
					System.out.println("Remote-Object " + fibonService.getName()
							+ " ist bereits vom Register durch den Client abgemeldet wurde.");
				} else {
					registry.unbind(fibonService.getName());
					UnicastRemoteObject.unexportObject((Remote) fibonService, false);
				}
			}

			nams = registry.list();
			System.out.print("Liste der registrierte Remote-Objecte vor dem Beenden:");

			if (nams.length > 0) {
				System.out.print("[ ");
				for (int i = 0; i < nams.length; i++) {
					System.out.print(" " + nams[i] + ", ");
				}
				System.out.println(" ] ");
			} else {
				System.out.println(" keine ! ");
			}


		} catch (Exception re) {
			System.out.println("Exception is thrown " + re.getMessage());
		}


		System.out.println("A32RmiServer beendet sich jetzt..");
	}
}
