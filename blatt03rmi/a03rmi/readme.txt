Autor: Oraz Serdarov
Verteilte Systeme SS 2020
Blatt 03: Java-RMI-Programmierung
Aufgabe 03
Vorbedingung:
Das aktuelle Arbeitsverzeichnis ist
blatt03rmi/a03rmi

Generieranleitung:
1. Generieren des ausführbaren Programms mit
javac -d build/classes/ src/rmilernen/tokenfibon/*.java

Installationsanleitung:
Entfällt. Die Dateien sind direkt nach Generierung ausführbar.

Bedienungsanleitung:


1. Start des des Servers und Registry
java -cp build/classes/ rmilernen/tokenfibon/A33RmiTokenFibonServer 10203




3. Start des des Clients
java -cp build/classes/ rmilernen/tokenfibon/A33RmiTokenFibonClient <par>

Parmater:
<par> eine Zahl für Fibonacci Methder




