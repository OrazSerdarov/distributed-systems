package rmilernen.tokenfibon;

import java.io.Serializable;
import java.rmi.RemoteException;


public class EinfachVerketteteListe implements  Serializable{
	public static final long serialVersionUID=5967311L;
	
	private ListElement head =null;

	public EinfachVerketteteListe()throws RemoteException{
		this.head = null;
	}

	public EinfachVerketteteListe(ListElement a) throws RemoteException {
		this.head = a;
	}

	public boolean isEmpty() {
		if (head == null) {
			return true;
		} else {
			return false;
		}
	}

	public boolean removeLast() {
		if (head != null) {
			ListElement lastElement = this.head;
			ListElement nextElement = lastElement.getNextElement();
			while (nextElement != null) {
				lastElement = nextElement;
				nextElement = lastElement.getNextElement();
			}
			lastElement = null;
			return true;

		} else {
			return false;
		}
	}

	public boolean removeFirst() {
		if (head != null) {
			this.head = head.getNextElement();
			return true;
		} else {
			return false;
		}
	}

	public void addListElement(ListElement le) {
		if (this.head == null) {
			this.head = le;
		} else {
			this.getLast().setNextElement(le);

		}
	}

	public ListElement getLast() {
		if (head != null) {
			ListElement lastElement = this.head;
			ListElement nextElement = lastElement.getNextElement();
			while (nextElement != null) {
				lastElement = nextElement;
				nextElement = lastElement.getNextElement();
			}
			return lastElement;

		} else {
			return null;
		}
	}

	public ListElement getFirst() {
		return head;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		ListElement e = head;
		while(e!= null) {
			sb.append(e.toString() +  " \n");
			e = e.getNextElement();
		}
		return sb.toString();
	}

}
