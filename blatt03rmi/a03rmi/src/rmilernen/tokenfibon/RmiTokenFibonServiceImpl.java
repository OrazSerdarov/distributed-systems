package rmilernen.tokenfibon;


import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class RmiTokenFibonServiceImpl implements RmiTokenFibonService {

	private String name;

	public String getName() {
		return name;
	}

	public RmiTokenFibonServiceImpl(String name) throws RemoteException {
		this.name = name;
	}

	@Override
	public long rmiFibon(int i) throws RemoteException {
		if (i < 0) {
			return 0;
		} else {
			return this.fibonacci(i + 1);
		}
	}

	@Override
	public Long[] isrmiFibonArray(int i) throws RemoteException {
		System.out.println("Client ruft : isrmiFibonArray()");

		if (i < 1) {
			return new Long[1];
		} else {

			int lastFibNum = i + 1;
			Long ret[] = new Long[numOfFib(lastFibNum)];
			int j = 1;
			int a = 0;
			long fib = fibonacci(j);
			while (fib <= lastFibNum) {

				if (!contains(ret, fib) || j == 1 || j == 2) {
					ret[a++] = new Long(fib);

				}
				fib = fibonacci(j++);
			}
			return ret;
		}
	}

	@Override
	public EinfachVerketteteListe rmiConvertToList(Long[] arr) throws RemoteException {
		System.out.println("Client ruft auf : rmiConvertToList()");

		if (arr.length <= 0) {
			return null;
		} else {

			EinfachVerketteteListe evl = new EinfachVerketteteListe();
			for (int i = 0; i < arr.length; i++) {
				evl.addListElement(new ListElement(arr[i], i));
			}

			return evl;
		}
	}

	@Override
	public String rmiServernameAt(int i) throws RemoteException {
		if (0 > i || i >= this.getClass().getSimpleName().length()) {
			return "-1";
		} else {
			return Character.toString(this.getClass().getSimpleName().charAt(i));
		}
	}

	@Override
	public int rmiStrlen(String s) throws RemoteException {

		if (s != null) {
			return s.length();
		} else {
			return -1;

		}
	}

	@Override
	public String rmiFirstToken(String s, char c) throws RemoteException {
		System.out.println("Client ruft auf : rmiFirstToken(" + s + "," + c + ")");

		if (s != null && s.indexOf(c) >= 0) {
			return s.substring(0, s.indexOf(c));
		} else {
			return s;
		}
	}

	@Override
	public StrAndIndex rmiRestAndIndexByChar(String s, char c) throws RemoteException {
		System.out.println("Client ruft : rmiRestAndIndexByChar(" + s + "," + c + ")");
		StrAndIndex o = new StrAndIndex("", -1);

		if (s != null && s.indexOf(c) >= 0) {
			int index = s.indexOf(c);
			o.setStr(s.substring(index + 1, s.length() - 1));
			o.setIndex(index);
			return o;
		} else {
			return o;
		}
	}

	@Override
	public StrStr rmiSplitAt(String s, int i) throws RemoteException {
		System.out.println("Client ruft : String s, int i(" + s + "," + i + ")");
		StrStr o = new StrStr("", "");
		if (0 < i && i < s.length()) {
			o.setPart1(s.substring(0, i));
			o.setPart2(s.substring(i, s.length()));
			return o;
		} else if (i < 0) {
			o.setPart2(s);
			return o;

		} else {
			o.setPart1(s);
			return o;
		}
	}

	@Override
	public void rmiIncrement(long l) throws RemoteException {
		System.out.println("Client ruft : rmiIncrement(" + l + ")");

		l++;
	}

	@Override
	public void rmiShutdown() throws RemoteException {
		System.out.println("Client ruft : rmiShutdown()");

		try {
			UnicastRemoteObject.unexportObject((Remote) this, false);
			Naming.unbind(this.name);
			Registry r= LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
			String nams[] = r.list();	
			for (int i = 0; i < nams.length; i++) {
				Naming.unbind(nams[i]);
			}
			nams =r.list();
			System.out.println("Liste der registrierter Remote-Objekte:");
		 if (nams.length > 0) {
				System.out.print("[ ");
				for (int i = 0; i < nams.length; i++) {
					Naming.unbind(nams[i]);
					System.out.print(" " + nams[i] + ", ");
				}
				System.out.println(" ] ");
			} else {
				System.out.println(" keine ! ");
			}
			System.exit(1);
 		} catch (Exception e) {
			System.out.println("Exception is thrown while trying to execute shut down" + e.getStackTrace());
		}
		
	}

	@Override
	public String rmiServernameAtWithException(int i) throws RemoteException {
		System.out.println("Client ruft :rmiServernameAtWithException(" + i + ")");
		return Character.toString(this.getClass().getSimpleName().charAt(i));
	}

	private int fibonacci(int a) {

		if (a == 1 || a == 2)
			return 1;

		else
			return fibonacci(a - 1) + fibonacci(a - 2);

	}

	private int numOfFib(long fibNum) {
		int ret = 0;
		int i = 1;
		long fib = fibonacci(i);
		long tempFib;
		while (fibNum >= fib) {
			tempFib = fib;
			fib = fibonacci(++i);
			if (tempFib != fib || fib == 1) {
				ret++;
			}
		}
		return ret;

	}

	private boolean contains(Long[] arr, long i) {
		for (int j = 0; j < arr.length; j++) {
			if (arr[j] == null) {
				break;
			} else if (arr[j].longValue() == i) {
				return true;
			}

		}
		return false;
	}

}
