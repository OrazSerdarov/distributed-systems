package rmilernen.tokenfibon;

import java.io.IOException;
import java.rmi.Naming;
import java.util.Scanner;


public class A33RmiTokenFibonClient {

	public static void main(String args[]) {
		System.out.println("Diese Lösung wurde erstellt von Oraz Serdarov\n");
		System.out.println("A32RmiClient wird gestartet...");

		String tokensServiceName = "rmi://localhost/" + RmiTokensService.class.getSimpleName();
		String fibonServiceName = "rmi://localhost/" + RmiFibonService.class.getSimpleName();
		String tokenFibonServiceName = "rmi://localhost/" + RmiTokenFibonService.class.getSimpleName();

		int fibonI = 6;
		Long[] arr = {};
		if (args.length > 0) {
			fibonI = Integer.valueOf(args[0]);
		}

		try {
			RmiTokensService tokensService = (RmiTokensService) Naming.lookup(tokensServiceName);
			RmiFibonService fibonService = (RmiFibonService) Naming.lookup(fibonServiceName);
			RmiTokenFibonService tokenFibonService = (RmiTokenFibonService) Naming.lookup(tokenFibonServiceName);

			
			//---------------------
			System.out.println("\t==== Aufrufe auf RmiTokensService ====");
			System.out.println("rmiServernameAt(2). Ergebnis:" + tokensService.rmiServernameAt(2));
			System.out.println("rmiStrlen('Hallo Welt!'). Ergebnis:" + tokensService.rmiStrlen("Hallo Welt!"));
			System.out.println(
					"rmiFirstToken('Erdbeeren', 'e'). Ergebnis:" + tokensService.rmiFirstToken("Erdbeeren", 'e'));
			System.out.println("rmiRestAndIndexByChar('Karottensalat', 'r'). Ergebnis: "
					+ tokensService.rmiRestAndIndexByChar("Karottensalat", 'r'));
			System.out.println("rmiSplitAt('Apfel', 3). Ergebnis: " + tokensService.rmiSplitAt("Apfel", 3));
			System.out.println("rmiIncrement(55). ");
			tokensService.rmiIncrement(55);
			System.out.println(
					"rmiServernameAtWithException(2). Ergebnis: " + tokensService.rmiServernameAtWithException(2));

			
			try {
				System.out.println(tokensService.rmiServernameAtWithException(20));
			} catch (Exception e) {
				System.out.println("rmiServernameAtWithException(20)). Ergebnis: \n" + e.getMessage() + "\n");
			}

			//-------------------------
			
			System.out.println("\n\t==== Aufrufe auf RmiFibonService ====");
			System.out.println("rmiFibon(" + fibonI + "). Ergebnis: " + fibonService.rmiFibon(fibonI));
			System.out.println("isrmiFibonArray(" + fibonI + "). Ergebnis: ");
			arr = fibonService.isrmiFibonArray(fibonI);
			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i] + " ");
			}
			System.out.println("\nrmiConvertToList(arr). Ergebnis: \n\n" + fibonService.rmiConvertToList(arr).toString());

			
			
			//---------------------------
			System.out.println("\n\t====Aufrufe auf RmiTokenFibonService ====");

			System.out.println("rmiServernameAt(2). Ergebnis:" + tokenFibonService.rmiServernameAt(2));
			System.out.println("rmiStrlen('Hallo Welt!'). Ergebnis:" + tokenFibonService.rmiStrlen("Hallo Welt!"));
			System.out.println(
					" rmiFirstToken('Erdbeeren', 'e'). Ergebnis:" + tokenFibonService.rmiFirstToken("Erdbeeren", 'e'));
			System.out.println("rmiRestAndIndexByChar('Karottensalat', 'r'). Ergebnis: "
					+ tokensService.rmiRestAndIndexByChar("Karottensalat", 'r'));
			System.out.println("rmiSplitAt('Apfel', 3). Ergebnis: " + tokenFibonService.rmiSplitAt("Apfel", 3));
			System.out.println("rmiIncrement(55). ");
			tokenFibonService.rmiIncrement(55);
			System.out.println(
					"rmiServernameAtWithException(2). Ergebnis: " + tokenFibonService.rmiServernameAtWithException(2));
			System.out.println("rmiServernameAtWithException(20)). Ergebnis:");
			System.out.println(tokenFibonService.rmiServernameAtWithException(20));
			System.out.println("rmiFibon(" + fibonI + "). Ergebnis: " + tokenFibonService.rmiFibon(fibonI));
			System.out.println("isrmiFibonArray(" + fibonI + "). Ergebnis: ");
			arr = tokenFibonService.isrmiFibonArray(fibonI);
			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i] + " ");
			}
			System.out.println(
					"\nrmiConvertToList(arr). Ergebnis: \n" + tokenFibonService.rmiConvertToList(arr).toString());



			try (Scanner sc = new Scanner(System.in)) {

				String input = "";

				System.out.println("Geben Sie die Zeichenkette 'quit' um den Server zu beenden:");
				input = sc.nextLine();
				if ("quit".equalsIgnoreCase(input.trim())) {
					System.out.println("rmiShutdown(). ");
					tokenFibonService.rmiShutdown();
				}
			} catch (IOException e) {

			}
		} catch (Exception e) {
			System.out.println("Exception " + e.getStackTrace());
		}


	}

}
