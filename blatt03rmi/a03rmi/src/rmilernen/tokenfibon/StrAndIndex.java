package rmilernen.tokenfibon;

import java.io.Serializable;

public class StrAndIndex implements Serializable {
	
	public static final long serialVersionUID=59607421L;
	private String str;
	private int index;


	public StrAndIndex(String s, int i) {
		this.str=s;
		this.index =i;
	}

	public int getIndex() {
		return index;
	}


	public void setIndex(int index) {
		this.index = index;
	}


	public String getStr() {
		return str;
	}


	public void setStr(String str) {
		this.str = str;
	}

	
	public String toString() {
		return "Str: " +this.getStr() + " Index: " +this.getIndex();
 	}




}
