package rmilernen.tokenfibon;


import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

public class RmiTokensServiceImpl  implements RmiTokensService {

	private String name;

	public String getName() {
		return name;
	}

	public RmiTokensServiceImpl(String name ) {
		this.name = name;

	}

	@Override
	public String rmiServernameAt(int i) throws RemoteException {
		System.out.println("Client ruft: rmiServernameAt("+i+ ")");

		if (0 > i || i >= this.getClass().getSimpleName().length()) {
			return "-1";
		} else {
			return Character.toString(this.getClass().getSimpleName().charAt(i));
		}
	}

	@Override
	public int rmiStrlen(String s) throws RemoteException {
		System.out.println("Client ruft: rmiStrlen("+s+ ")");

		if (s != null) {
			return s.length();
		} else {
			return -1;

		}
	}

	@Override
	public String rmiFirstToken(String s, char c) throws RemoteException {
		System.out.println("Client ruft: rmiFirstToken(" + s + ","+ c +")" );

		if (s != null && s.indexOf(c) >= 0) {
			return s.substring(0, s.indexOf(c));
		} else {
			return s;
		}
	}

	@Override
	public StrAndIndex rmiRestAndIndexByChar(String s, char c) throws RemoteException {
		System.out.println("Client ruft: rmiRestAndIndexByChar(" + s + ","+ c +")" );
		StrAndIndex o = new StrAndIndex("", -1);

		if (s != null && s.indexOf(c) >= 0) {
			int index = s.indexOf(c);
			o.setStr(s.substring(index + 1, s.length() - 1));
			o.setIndex(index);
			return o;
		} else {
			return o;
		}
	}

	@Override
	public StrStr rmiSplitAt(String s, int i) throws RemoteException {
		System.out.println("Client ruft: String s, int i(" + s + ","+ i +")" );
		StrStr o = new StrStr("", "");
		if (0 < i && i < s.length()) {
			o.setPart1(s.substring(0, i));
			o.setPart2(s.substring(i , s.length()));
			return o;
		} else if (i < 0) {
			o.setPart2(s);
			return o;

		} else {
			o.setPart1(s);
			return o;
		}
	}

	@Override
	public void rmiIncrement(long l) throws RemoteException {
		System.out.println("Client ruft: rmiIncrement("+l+")" );

		l++;

	}

	@Override
	public void rmiShutdown() throws RemoteException {
		System.out.println("Client ruft: rmiShutdown()");

		try {
			UnicastRemoteObject.unexportObject((Remote) this, false);
			Naming.unbind(this.name);
			System.exit(1);
		} catch (Exception e) {
			System.out.println("Exception is thrown while trying to execute shut down" + e.getStackTrace());
		}
	}

	@Override
	public String rmiServernameAtWithException(int i) throws RemoteException {
		System.out.println("Client ruft: rmiServernameAtWithException(" +i+")");
		return Character.toString(this.getClass().getSimpleName().charAt(i));
		
	}

}
