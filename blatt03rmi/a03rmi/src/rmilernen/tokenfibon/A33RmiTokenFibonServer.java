package rmilernen.tokenfibon;

import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RemoteServer;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;


public class A33RmiTokenFibonServer {

	public static void main(String args[]) {
		System.out.println("Diese Lösung wurde erstellt von Oraz Serdarov\n");
		System.out.println("A33RmiTokenFibon wird gestartet...");

		RmiTokensServiceImpl tokensService;
		RmiFibonServiceImpl fibonService;
		RmiTokenFibonServiceImpl tokenFibonService;
		Registry registry;
		String nams[] = {};
		
		System.out.println("Aktuelles Arbeitsverzeichnis ist :" + System.getProperty("user.dir"));
		try {
			// temp
			LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
			registry = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);

			tokensService = new RmiTokensServiceImpl(RmiTokensService.class.getSimpleName());
			fibonService = new RmiFibonServiceImpl(RmiFibonService.class.getSimpleName());
			tokenFibonService = new RmiTokenFibonServiceImpl(RmiTokenFibonService.class.getSimpleName());


			//RemoteServer.setLog(System.out);
			RmiTokensService stubToken = (RmiTokensService) UnicastRemoteObject.exportObject(tokensService, 0);
			RmiFibonService stubFibon = (RmiFibonService) UnicastRemoteObject.exportObject(fibonService, 0);
			RmiTokenFibonService stubTokenFibon = (RmiTokenFibonService) UnicastRemoteObject.exportObject(tokenFibonService, 0);


			registry.rebind(tokensService.getName(), stubToken);
			registry.rebind(fibonService.getName(), stubFibon);
			registry.rebind(tokenFibonService.getName(), stubTokenFibon);
			System.out.println("Registierte Remote-Objekte :");

			nams = registry.list();
			for (int i = 0; i < nams.length; i++) {
				System.out.println((i + 1) + ". " + nams[i]);
			}

			System.out.println("Geben Sie die Zeichenkette 'quit' um den Server zu beenden:");

			Scanner sc = new Scanner(System.in);
			String input = sc.nextLine();

			if ("quit".equalsIgnoreCase(input)) {
				UnicastRemoteObject.unexportObject((Remote) tokensService, true);
				registry.unbind(tokensService.getName());
				UnicastRemoteObject.unexportObject((Remote) fibonService, true);
				registry.unbind(fibonService.getName());
				UnicastRemoteObject.unexportObject((Remote) tokenFibonService, true);
				registry.unbind(tokenFibonService.getName());
			} else {
				System.out.println("Remote-Object " + tokensService.getName()
						+ " ist bereits vom Register durch den Client abgemeldet wurde.");
			}
			nams = registry.list();

		} catch (Exception re) {
			System.out.println("Exception is thrown: " + re.getMessage());
		}

		try {
			registry = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
			nams = registry.list();
		} catch (Exception ee) {
		}
		System.out.print("Liste der registrierte Remote-Objecte vor dem Beenden:");

		if (nams.length > 0) {
			System.out.print("[ ");
			for (int i = 0; i < nams.length; i++) {
				System.out.print(" " + nams[i] + ", ");
			}
			System.out.println(" ] ");
		} else {
			System.out.println(" keine ! ");
		}

		System.out.println("A31RmiServer beendet sich jetzt..");
	}

}
