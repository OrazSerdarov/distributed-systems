package rmilernen.tokenfibon;


import java.io.Serializable;
import java.rmi.RemoteException;

public class ListElement  implements Serializable{

	public static final long serialVersionUID=999513301L;
	
	private long value;
	private int index;
	 private ListElement nextElement;

	
	public ListElement(long v , int i) throws RemoteException {
		this.value=v;
		this.index=i;
	}

	public ListElement() throws RemoteException {
		this.nextElement = null;
	}

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public ListElement getNextElement() {
		return nextElement;
	}

	public void setNextElement(ListElement nextElement) {
		this.nextElement = nextElement;
	}
	
	public String toString() {
		String ret = "Index: " +this.getIndex() + ", Value: " +this.getValue() + ", Next: ";
				if(this.getNextElement()==null) {
					ret+="null";
				}else {
					ret+="ListElement";
				}
		return ret;
	}
}
