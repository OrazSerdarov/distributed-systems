package rmilernen.tokenfibon;

import java.io.Serializable;

public class StrStr implements Serializable{
	public static final long serialVersionUID=28959301L;

	private String part1;
	private String part2;
	
	public StrStr(String s1 , String s2) {
		this.part1 =s1;
		this.part2=s2;
	}
	public String getPart1() {
		return part1;
	}
	public void setPart1(String part1) {
		this.part1 = part1;
	}
	public String getPart2() {
		return part2;
	}
	public void setPart2(String part2) {
		this.part2 = part2;
	}
	public String toString() {
		return "Teil1: " +this.getPart1() + " Teil2 :" +this.getPart2();
	}
}
