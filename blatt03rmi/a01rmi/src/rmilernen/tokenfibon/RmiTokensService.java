package rmilernen.tokenfibon;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RmiTokensService extends Remote{

	public String rmiServernameAt(int i) throws RemoteException;

	public int rmiStrlen(String s) throws RemoteException;

	public String rmiFirstToken(String s, char c) throws RemoteException;

	public StrAndIndex rmiRestAndIndexByChar(String s, char c) throws RemoteException;

	public StrStr rmiSplitAt(String s, int i) throws RemoteException;
	
	public void rmiIncrement(long l) throws RemoteException;
	
	public void rmiShutdown() throws RemoteException;
	
	public String rmiServernameAtWithException(int i) throws RemoteException; //Ausnahme werfen

}
