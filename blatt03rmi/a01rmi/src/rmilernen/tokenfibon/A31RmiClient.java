package rmilernen.tokenfibon;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.Scanner;

public class A31RmiClient {

	public static void main(String args[]) {
		System.out.println("Diese Lösung wurde erstellt von Oraz Serdarov\n");
		System.out.println("A31RmiClient wird gestartet...");
		String remoteObjName = "rmi://localhost/" + RmiTokensService.class.getSimpleName();

		if (args.length > 0) {
			remoteObjName = args[0] + RmiTokensService.class.getSimpleName();
		}

		System.out.println("Ziel der RMI-Aufrufe ist: " + remoteObjName + "\n");
		RmiTokensService tokens = null;
		try {
			tokens = (RmiTokensService) Naming.lookup(remoteObjName);
			System.out.println("rmiServernameAt(2). Ergebnis:" + tokens.rmiServernameAt(2));
			System.out.println("rmiStrlen('Hallo Welt!'). Ergebnis:" + tokens.rmiStrlen("Hallo Welt!"));
			System.out.println("rmiFirstToken('Erdbeeren', 'e'). Ergebnis:" + tokens.rmiFirstToken("Erdbeeren", 'e'));
			System.out.println("rmiRestAndIndexByChar('Karottensalat', 'r'). Ergebnis: "
					+ tokens.rmiRestAndIndexByChar("Karottensalat", 'r'));
			System.out.println("rmiSplitAt('Apfel', 3). Ergebnis: " + tokens.rmiSplitAt("Apfel", 3));
			System.out.println("rmiIncrement(55). ");
			tokens.rmiIncrement(55);
			System.out.println("rmiServernameAtWithException(2). Ergebnis: " + tokens.rmiServernameAtWithException(2));
			System.out.print("rmiServernameAtWithException(20)). Ergebnis:");
			System.out.println(tokens.rmiServernameAtWithException(20));

		} catch (IndexOutOfBoundsException ee) {
			System.out.println("\n Exception is occured while executing remote method rmiServernameAtWithException()"
					+ ee.getMessage());

		} catch (Exception e) {
			System.out.println("Exception bei rmiShutdown()" + e.getMessage());
		}
		
		
		System.out.println("Geben Sie die Zeichenkette 'quit' um den Server zu beenden:");
		try (Scanner sc = new Scanner(System.in)) {
			String in = sc.nextLine();
			if (tokens != null && "quit".equalsIgnoreCase(in.trim())) {
			
				tokens.rmiShutdown();
	
			}

		} catch (RemoteException e) {
			System.out.println("RemoteException bei shutdown \n" + e.getMessage());
		}

		System.out.println("A31RmiClient beendet sich jetzt..");
	}

}
