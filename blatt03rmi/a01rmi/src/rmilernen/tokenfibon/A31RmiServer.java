package rmilernen.tokenfibon;

import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

public class A31RmiServer {

	public static void main(String args[]) {
		System.out.println("Diese Lösung wurde erstellt von Oraz Serdarov\n");
		System.out.println("A31RmiServer wird gestartet...");

		String input;
		String nams[] = {};
		RmiTokensServiceImpl tokensService;
		Registry registry;
		
		String remoteObjName = RmiTokensService.class.getSimpleName();
		System.out.println("Aktuelles Arbeitsverzeichnis ist :" + System.getProperty("user.dir"));
		try(Scanner sc = new Scanner(System.in)) {
			// temp
			LocateRegistry.createRegistry(Registry.REGISTRY_PORT);

			tokensService = new RmiTokensServiceImpl(remoteObjName);
			RmiTokensService stub = (RmiTokensService) UnicastRemoteObject.exportObject(tokensService, 0);
			
			// RemoteServer.setLog(System.out);
			registry = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
			
			
			registry.rebind(tokensService.getName(), stub);
			System.out.println("Remote-Objekt(" +tokensService.getName() +") wurde beim Naming-Service regestriet!");

			nams = registry.list();
			for (int i = 0; i < nams.length; i++) {
				System.out.println((i + 1) + ". ergistrierter Remote-Object : " + nams[i]);
			}

			System.out.println("Geben Sie die Zeichenkette 'quit' um den Server zu beenden:");
		
			input = sc.nextLine();



			if ("quit".equalsIgnoreCase(input)) {
				UnicastRemoteObject.unexportObject((Remote) tokensService, true);
				registry.unbind(tokensService.getName());
			}else {
				System.out.println("Remote-Object(" + tokensService.getName()+") ist bereits vom Register durch den Client abgemeldet wurde.");
			}
			nams = registry.list();

		} catch (Exception re) {
			System.out.println("Exception is thrown: " + re.getMessage());
		}

		System.out.print("Liste der registrierte Remote-Objecte vor dem Beenden:");

		if (nams.length > 0) {
			System.out.print("[ ");
			for (int i = 0; i < nams.length; i++) {
				System.out.print(" " + nams[i] + ", ");
			}
			System.out.println(" ] ");
		} else {
			System.out.println(" keine ! ");
		}

		System.out.println("A31RmiServer beendet sich jetzt..");
	}

}
